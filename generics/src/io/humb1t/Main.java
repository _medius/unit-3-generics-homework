package io.humb1t;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Main {

	private static final int NUMBER_OF_ORDERS = 5;
	private static final int NUMBER_OF_ORDER_CHILDREN = 3;

	private OrderProcessor<Order> orderProcessor = new OrderProcessor<>();

	private List<Order> orderList = new ArrayList<>();
	private List<OrderChild> orderChildList = new ArrayList<>();

	private void dataInit() {
		Random random = new Random();
		OrderStatus randomStatus;

		for (int i = 0; i < NUMBER_OF_ORDERS; i++) {
			randomStatus = OrderStatus.values()[random.nextInt(OrderStatus.values().length)];
			orderList.add(new Order(random.nextInt(), randomStatus));
		}

		for (int i = 0; i < NUMBER_OF_ORDER_CHILDREN; i++) {
			randomStatus = OrderStatus.values()[random.nextInt(OrderStatus.values().length)];
			orderChildList.add(new OrderChild(random.nextInt(), randomStatus));
		}
	}

	private <T> void proceedOrders(List<? extends Order> ordersToProceed) {
		for (Order order : ordersToProceed) {
			switch (order.getStatus()) {
				case NOT_STARTED:
					orderProcessor.start(order);
					break;

				case PROCESSING:
					orderProcessor.complete(order);
					break;

				case COMPLETED:
					orderProcessor.fail(order);
					break;

				default:
					System.out.println("Order " + order.getId() + " has ERROR status, " + order.getClass());
			}
		}
	}

	public void appStart() {
		dataInit();

		proceedOrders(orderList);
		proceedOrders(orderChildList);
	}

	public static void main(String[] args) {
		new Main().appStart();
	}
}
