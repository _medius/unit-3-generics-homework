package io.humb1t;

public class OrderProcessor<T extends Order> {

	public void start(T order) {
		if (!OrderStatus.NOT_STARTED.equals(order.getStatus())) {
			throw new IllegalStateException("Order should be in NOT_STARTED state.");
		}
		order.setStatus(OrderStatus.PROCESSING);
		System.out.println("Order " + order.getId() + " STARTED, " + order.getClass());
	}

	public void complete(T order) {
		if (OrderStatus.NOT_STARTED.equals(order.getStatus())) {
			throw new IllegalStateException("Order should be in PROCESSING state.");
		}
		order.setStatus(OrderStatus.COMPLETED);
		System.out.println("Order " + order.getId() + " COMPLETED, " + order.getClass());
	}

	public void fail(T order) {
		order.setStatus(OrderStatus.ERROR);
		System.out.println("Order " + order.getId() + " FAILED, " + order.getClass());
	}
}
