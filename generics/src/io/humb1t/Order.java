package io.humb1t;

public class Order {

    private static int ID_SEQUENCE = 0;

    {
        id = ++ID_SEQUENCE;
    }

    private final int id;
    private final int totalCost;
    private OrderStatus status;

    public Order(int totalCost, OrderStatus status) {
        this.totalCost = totalCost;
        this.status = status;
    }

    public void start() {
        if (!OrderStatus.NOT_STARTED.equals(this.status)) {
            throw new IllegalStateException("Order should be in NOT_STARTED state.");
        }
        this.status = OrderStatus.PROCESSING;
    }

    public boolean isReady() {
        return OrderStatus.COMPLETED.equals(this.status);
    }

    public boolean isFailed() {
        return OrderStatus.ERROR.equals(this.status);
    }

    public int getTotalCost() {
        return totalCost;
    }

    public OrderStatus getStatus() {
        return status;
    }

    public void setStatus(OrderStatus status) {
        this.status = status;
    }

    public int getId() {
        return id;
    }
}
