I created OrderProcessor which provides methods to change Order status.
I have also create OrderChild class to test support the subtypes of
Order by OrderProcessor.
In Main class method proceedOrders change Order status depends on it
current status using OrderProcessor and print result into console.